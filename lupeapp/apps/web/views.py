from django.shortcuts import render, redirect
from apps.projects.models import *
# Create your views here.


def indexView(request):
    return render(request, 'login.html')


def projectView(request):
    if request.GET["code"]:
        code = request.GET["code"]
        projecto = Project.objects.get(cod=code)
        actividades = Activity.objects.filter(project=projecto).order_by('-date')
        empresa = Empresa.objects.get(pk=projecto.empresa.id)
        data = {'empresa': empresa, 'projecto': projecto, 'actividades': actividades}
        return render(request, 'project.html', data)
    else:
        return redirect('indexView')
