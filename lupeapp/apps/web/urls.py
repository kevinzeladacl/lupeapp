from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.indexView, name='IndexView'),
    url(r'^projecto/$', views.projectView, name='projectView'),
]
