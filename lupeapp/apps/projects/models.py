from django.db import models

# Create your models here.


class Empresa(models.Model):
    nombre = models.CharField(max_length=140)
    rut = models.CharField(max_length=140)
    logo = models.FileField(upload_to='uploads', blank=True)
    contacto = models.CharField(max_length=140)
    email_contacto = models.EmailField(max_length=140)
    token = models.CharField(max_length=140)

    def __str__(self):
        return self.nombre


class Project(models.Model):
    nombre = models.CharField(max_length=140)
    empresa = models.ForeignKey(Empresa)
    estado = models.IntegerField(default=0)
    detalle = models.CharField(max_length=140)
    cod = models.CharField(max_length=140)

    def __str__(self):
        return self.nombre


class Activity(models.Model):
    project = models.ForeignKey(Project)
    type_activity = models.IntegerField(default=0)
    detalle = models.CharField(max_length=140)
    adjunto = models.FileField(upload_to='uploads')
    date = models.DateTimeField(auto_now_add=True)
