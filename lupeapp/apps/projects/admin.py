from django.contrib import admin
from .models import Empresa, Project, Activity
# Register your models here.


@admin.register(Empresa)
class EmpresaAdmin(admin.ModelAdmin):
    list_display = (
        'nombre',
        'rut',
        'contacto',
        'email_contacto',
    )


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = (
        'nombre',
        'empresa',
        'estado',
        'detalle',
    )


@admin.register(Activity)
class ActivityAdmin(admin.ModelAdmin):
    list_display = (
        'project',
        'type_activity',
        'adjunto',
    )
